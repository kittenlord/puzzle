package kittenlord.puzzle;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * State of the game: the plase, and positions of the pieces.
 */
public class GameState {

	public enum Phase {
		/**
		 * The image is cut into pieces (so the player sees both the picture and the game resolution), but the game has
		 * not started yet.
		 */
		PREVIEW,
		/**
		 * The game is in progress; the piece originally in the lowest-rightmost corner is missing, the remaining pieces
		 * are being moved by the player.
		 */
		PLAYING,
		/**
		 * The game was completed; the original picture is displayed intact.
		 */
		COMPLETED
	}

	private final int resolutionX;
	private final int resolutionY;

	private Phase phase = Phase.PREVIEW;

	/**
	 * Positions of the image pieces on the screen. The key is a coordinate on the screen, i.e. (0,0) refers to the
	 * upper left corner of the canvas. The value is a coordinate within the image, i.e. (0,0) refers to the upper left
	 * piece of the image. There is no entry for the empty position on the screeen, which is in {@link #emptyPosition}.
	 */
	private final Map<Position, Position> piecePositions = new HashMap<>();

	/**
	 * Screen position that does not contain a piece; the key missing in {@link #piecePositions}.
	 */
	private Position emptyPosition;

	/**
	 * Creates a new game state with given resolution. All pieces are in their original positions, the lowest-rightmost
	 * one is missing.
	 */
	public GameState(int resolutionX, int resolutionY) {
		if ((resolutionX < 2) || (resolutionY < 2)) {
			throw new IllegalArgumentException("Game resolution must be at least 2×2.");
		}
		this.resolutionX = resolutionX;
		this.resolutionY = resolutionY;
		piecePositions.clear();
		for (var coordinate : Position.rectangle(resolutionX, resolutionY)) {
			piecePositions.put(coordinate, coordinate);
		}
		piecePositions.remove(emptyPosition = new Position(resolutionX - 1, resolutionY - 1));
	}

	/**
	 * Returns the game state described by the string, or throws a runtime exception if the data are invalid. Some
	 * effort was made to detect an invalid input, but the detection is not perfect; if you succeed to import invalid
	 * data, you may create an unsolvable puzzle.
	 * 
	 * @throw NumberFormatException the input is not a list of comma-separated integers
	 * @throw IllegalArgumentException the data in the list do not describe a valid position
	 */
	public static GameState importData(String exportedData) {
		var data = Arrays.asList(exportedData.split(",")).stream()
				.map(s -> Integer.parseInt(s))
				.collect(Collectors.toCollection(LinkedList::new));
		int rx = data.remove();
		int ry = data.remove();
		var state = new GameState(rx, ry);
		state.phase = Phase.PLAYING; // game can only be saved in this phase
		state.piecePositions.clear();
		state.emptyPosition = null;
		for (var coordinate : Position.rectangle(rx, ry)) {
			int x = data.remove();
			int y = data.remove();
			if ((x < 0) && (y < 0)) {
				if (null != state.emptyPosition) {
					throw new IllegalArgumentException("Multiple empty positions");
				}
				state.emptyPosition = coordinate;
			} else {
				if ((x < 0) || (x >= rx) || (y < 0) || (y >= ry)) {
					throw new IllegalArgumentException("Invalid piece: (" + x + "," + y + ")");
				}
				var piece = new Position(x, y);
				if (state.piecePositions.containsValue(piece)) {
					throw new IllegalArgumentException("Duplicate piece: (" + x + "," + y + ")");
				}
				state.piecePositions.put(coordinate, piece);
			}
		}
		if (null == state.emptyPosition) {
			throw new IllegalArgumentException("No empty position");
		}
		if (!data.isEmpty()) {
			throw new IllegalArgumentException("Too many numbers in: " + exportedData);
		}
		return state;
	}

	/**
	 * Describes the game state by a string. The string is a comma-separated list of integers, starting with X and Y
	 * resolutions, followed by pieces for each position, with the empty position represented by numbers (-1,-1).
	 */
	public String exportData() {
		var data = new ArrayList<>();
		data.add(resolutionX);
		data.add(resolutionY);
		for (var coordinate : Position.rectangle(resolutionX, resolutionY)) {
			if (piecePositions.containsKey(coordinate)) {
				data.add(piecePositions.get(coordinate).getX());
				data.add(piecePositions.get(coordinate).getY());
			} else {
				data.add(-1);
				data.add(-1);
			}
		}
		return data.stream().map(i -> i.toString()).collect(Collectors.joining(","));
	}

	/**
	 * Draws the game state.
	 */
	public void drawTo(Graphics g, BufferedImage image, ImagePieces pieces) {
		if (null == image) {
			return;
		}
		if (Phase.COMPLETED == phase) {
			drawCompleteTo(g, image, pieces);
			return;
		}
		drawPiecesTo(g, pieces);
	}

	/**
	 * Draws a complete picture. The game area may be a little larger than the image, the extra pixels are also drawn.
	 */
	private void drawCompleteTo(Graphics g, BufferedImage image, ImagePieces pieces) {
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, pieces.getTotalWidth(), pieces.getTotalHeight());
		g.drawImage(image, 0, 0, null);
	}

	/**
	 * Draws all pieces, and an empty rectangle for the missing piece.
	 */
	private void drawPiecesTo(Graphics g, ImagePieces pieces) {
		for (var coordinate : Position.rectangle(resolutionX, resolutionY)) {
			int x = pieces.getPieceWidth() * coordinate.getX();
			int y = pieces.getPieceHeight() * coordinate.getY();
			if (Phase.PREVIEW == phase) {
				pieces.draw(g, x, y, coordinate);
			} else if (coordinate.equals(emptyPosition)) {
				g.setColor(Color.WHITE);
				g.fillRect(x, y, pieces.getPieceWidth(), pieces.getPieceHeight());
			} else {
				pieces.draw(g, x, y, piecePositions.get(coordinate));
			}
		}
	}

	public boolean isPlaying() {
		return Phase.PLAYING == phase;
	}

	public int getResolutionX() {
		return resolutionX;
	}

	public int getResolutionY() {
		return resolutionY;
	}

	/**
	 * Returns whether this coordinate corresponds to a position on the game canvas.
	 */
	private boolean isValid(Position c) {
		return (0 <= c.getX()) && (c.getX() < resolutionX) && (0 <= c.getY()) && (c.getY() < resolutionY);
	}

	public boolean canMove(Direction d) {
		if (Phase.PLAYING != phase) {
			return false;
		}
		var moved = emptyPosition.neighbor(d.opposite());
		return isValid(moved);
	}

	/**
	 * Tries to move a piece in given direction; returns true on success, false if the move is not possible.
	 */
	public boolean move(Direction d) {
		if (Phase.PLAYING != phase) {
			return false;
		}
		var moved = emptyPosition.neighbor(d.opposite());
		if (!isValid(moved)) {
			return false;
		}
		piecePositions.put(emptyPosition, piecePositions.remove(moved));
		emptyPosition = moved;
		if (isSolved()) {
			phase = Phase.COMPLETED;
		}
		return true;
	}

	/**
	 * Randomly moves the pieces of the picture in a way that allows the game to be solved.
	 * <p>
	 * To guarantee that the game can be solved, we must switch an even number of pairs. We achieve this by always
	 * taking three different pieces and rotating them like this: A -&gt; B -&gt; C -&gt; A, which is equivalent to
	 * switching A with B, and then A with C.
	 */
	public void shuffle() {
		phase = Phase.PLAYING;
		for (var first : Position.rectangle(resolutionX, resolutionY)) {
			Position second = Position.random(resolutionX, resolutionY);
			Position third = Position.random(resolutionX, resolutionY);
			if (first.equals(second) || first.equals(third) || second.equals(third)) {
				continue;
			}
			if (piecePositions.containsKey(first)
					&& piecePositions.containsKey(second)
					&& piecePositions.containsKey(third)) {
				Position tmp = piecePositions.remove(third);
				piecePositions.put(third, piecePositions.remove(second));
				piecePositions.put(second, piecePositions.remove(first));
				piecePositions.put(first, tmp);
			}
		}
		if (isSolved()) {
			// This is very unlikely, but possible -- with resolution 3×3, the chance is 1 in 20160, that is less than
			// 0.005%, and with greater resolution the chance is less than 1 in a million -- so we do not handle this
			// rare possibility in any way other than correctly noticing that it has happened.
			phase = Phase.COMPLETED;
		}
	}

	/**
	 * Returns whether all pieces are at their correct places.
	 */
	private boolean isSolved() {
		return piecePositions.entrySet().stream().allMatch(entry -> entry.getKey().equals(entry.getValue()));
	}

	/**
	 * A string representation of the piece positions, useful for debugging.
	 */
	@Override
	public String toString() {
		return IntStream.range(0, resolutionY)
				.mapToObj(y -> IntStream.range(0, resolutionX)
						.mapToObj(x -> {
							Position c = new Position(x, y);
							return piecePositions.containsKey(c) ? piecePositions.get(c).toString() : "#####";
						}).collect(Collectors.joining(",")))
				.collect(Collectors.joining("\n"));
	}

}
