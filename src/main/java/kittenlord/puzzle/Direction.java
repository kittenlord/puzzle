package kittenlord.puzzle;

/**
 * One of the four basic directions on screen.
 */
public enum Direction {
	UP(0, -1),
	RIGHT(1, 0),
	DOWN(0, 1),
	LEFT(-1, 0);

	private final int dx;
	private final int dy;

	private Direction(int dx, int dy) {
		this.dx = dx;
		this.dy = dy;
	}

	private static Direction of(int dx, int dy) {
		for (var d : values()) {
			if ((dx == d.dx) && (dy == d.dy)) {
				return d;
			}
		}
		throw new IllegalArgumentException("No such direction: " + dx + ", " + dy);
	}

	/**
	 * Horizontal component of the direction on screen.
	 */
	public int getX() {
		return dx;
	}

	/**
	 * Vertical component of the direction on screen.
	 */
	public int getY() {
		return dy;
	}

	/**
	 * The inverse direction, i.e. up/down, left/right.
	 */
	public Direction opposite() {
		return Direction.of(-dx, -dy);
	}

}
