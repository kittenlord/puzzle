package kittenlord.puzzle;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * A pair of X and Y values.
 */
public class Position {

	private static Random random = new Random();

	private final int x;
	private final int y;

	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Returns a list containing all positions in the rectangle of given size. The order of positions is unspecified,
	 * but always the same.
	 */
	public static List<Position> rectangle(int rangeX, int rangeY) {
		return IntStream.range(0, rangeY).boxed()
				.flatMap(y -> IntStream.range(0, rangeX).mapToObj(x -> new Position(x, y)))
				.collect(Collectors.toList());
	}

	/**
	 * Returns a random postion in rectangle of given size.
	 * <p>
	 * Note: This method is not thread-safe. We only call it from the AWT/Swing thread, so it does not matter.
	 */
	public static Position random(int rangeX, int rangeY) {
		return new Position(random.nextInt(rangeX), random.nextInt(rangeY));
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	/**
	 * Returns the next position in given direction.
	 */
	public Position neighbor(Direction d) {
		return new Position(x + d.getX(), y + d.getY());
	}

	@Override
	public final boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		if (!(object instanceof Position)) {
			return false;
		}
		var that = (Position) object;
		return (this.x == that.x) && (this.y == that.y);
	}

	@Override
	public int hashCode() {
		return 13 * x + 17 * y;
	}

	@Override
	public String toString() {
		return "(" + x + "," + y + ")";
	}

}
