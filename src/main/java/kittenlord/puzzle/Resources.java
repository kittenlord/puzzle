package kittenlord.puzzle;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

public class Resources {

	private static final String APPLICATION_ICON_FILE_NAME = "app.png";
	private static final String DEFAULT_IMAGE_FILE_NAME = "kittens.jpg";

	private static final Pattern END_OF_STREAM_WITHOUT_LAST_LINE_TERMINATOR = Pattern.compile("\\Z");

	public static BufferedImage getApplicationIcon() {
		return getImage(APPLICATION_ICON_FILE_NAME);
	}

	/**
	 * Returns the default puzzle image.
	 * 
	 * @see https://unsplash.com/photos/rplhB9mYF48
	 */
	public static BufferedImage getDefaultImage() {
		return getImage(DEFAULT_IMAGE_FILE_NAME);
	}

	private static BufferedImage getImage(String imageFileName) {
		try (var is = Resources.class.getResourceAsStream(imageFileName)) {
			if (null == is) {
				throw new IllegalStateException(String.format("Resource '%s' not found", DEFAULT_IMAGE_FILE_NAME));
			}
			return ImageIO.read(is);
		} catch (IOException e) {
			throw new IllegalStateException(String.format("Error loading resource '%s'", DEFAULT_IMAGE_FILE_NAME), e);
		}
	}

	/**
	 * Returns the help text, or an error message.
	 */
	public static String getHelpText() {
		try (var is = Resources.class.getResourceAsStream("help.txt")) {
			if (null == is) {
				return "Error: Help text not found";
			}
			try (var s = new Scanner(is)) {
				s.useDelimiter(END_OF_STREAM_WITHOUT_LAST_LINE_TERMINATOR);
				return s.next();
			}
		} catch (IOException e) {
			return "Error loading help text" + "\n" + e.getMessage();
		}
	}

}
