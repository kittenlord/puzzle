package kittenlord.puzzle;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

public class ImagePieces {

	private final int resolutionX;
	private final int resolutionY;
	private final int pieceWidth;
	private final int pieceHeight;

	private final Map<Position, BufferedImage> pieces = new HashMap<>();

	public ImagePieces(BufferedImage image, int resolutionX, int resolutionY) {
		this.resolutionX = resolutionX;
		this.resolutionY = resolutionY;
		pieceWidth = divRoundUp(image.getWidth(), resolutionX);
		pieceHeight = divRoundUp(image.getHeight(), resolutionY);
		pieces.clear();
		for (var coordinate : Position.rectangle(resolutionX, resolutionY)) {
			pieces.put(coordinate, cutPiece(image, coordinate.getX(), coordinate.getY()));
		}
	}

	static int divRoundUp(int denominator, int divisor) {
		return (denominator + divisor - 1) / divisor;
	}

	public void draw(Graphics g, int x, int y, Position piece) {
		g.drawImage(pieces.get(piece), x, y, null);
	}

	/**
	 * Returns an image containing a part of the provided image, with added borders.
	 */
	private BufferedImage cutPiece(Image image, int x, int y) {
		BufferedImage piece = new BufferedImage(pieceWidth, pieceHeight, BufferedImage.TYPE_INT_ARGB);
		Graphics g = piece.getGraphics();
		g.setColor(Color.BLACK);
		g.drawRect(0, 0, pieceWidth, pieceHeight);
		g.drawImage(image, -x * pieceWidth, -y * pieceHeight, null);
		g.setColor(Color.BLACK);
		g.drawLine(pieceWidth - 1, 0, pieceWidth - 1, pieceHeight - 1); // right border
		g.setColor(Color.WHITE);
		g.drawLine(0, 0, 0, pieceHeight - 1); // left border
		g.setColor(Color.BLACK);
		g.drawLine(0, pieceHeight - 1, pieceWidth - 1, pieceHeight - 1); // bottom border
		g.setColor(Color.WHITE);
		g.drawLine(0, 0, pieceWidth - 1, 0); // top border
		return piece;
	}

	public int getPieceWidth() {
		return pieceWidth;
	}

	public int getPieceHeight() {
		return pieceHeight;
	}

	public int getTotalWidth() {
		return resolutionX * pieceWidth;
	}

	public int getTotalHeight() {
		return resolutionY * pieceHeight;
	}

	public Dimension getTotalSize() {
		return new Dimension(getTotalWidth(), getTotalHeight());
	}

}
