package kittenlord.puzzle;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Dialog.ModalityType;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

/**
 * A puzzle game. An image is cut into rectangular pieces which are then shuffled. The player tries to arrange the
 * pieces in their original positions using keyboard.
 */
public class Game implements Runnable {

	/**
	 * Mapping the key codes onto the four basic directions.
	 */
	private static final Map<Integer, Direction> KEYS;

	private JFrame appFrame;
	private Canvas canvas;
	private JFileChooser fileChooser = new JFileChooser();

	private BufferedImage image;
	private ImagePieces pieces;
	private GameState state = new GameState(4, 4);

	static {
		KEYS = new HashMap<>();
		KEYS.put(KeyEvent.VK_UP, Direction.UP);
		KEYS.put(KeyEvent.VK_KP_UP, Direction.UP);
		KEYS.put(KeyEvent.VK_RIGHT, Direction.RIGHT);
		KEYS.put(KeyEvent.VK_KP_RIGHT, Direction.RIGHT);
		KEYS.put(KeyEvent.VK_DOWN, Direction.DOWN);
		KEYS.put(KeyEvent.VK_KP_DOWN, Direction.DOWN);
		KEYS.put(KeyEvent.VK_LEFT, Direction.LEFT);
		KEYS.put(KeyEvent.VK_KP_LEFT, Direction.LEFT);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Game());
	}

	@Override
	public void run() {
		appFrame = new JFrame("Puzzle Game");
		appFrame.setIconImage(Resources.getApplicationIcon());
		appFrame.add(canvas = createCanvas());
		appFrame.setJMenuBar(createMenu());
		appFrame.pack();
		appFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		appFrame.setVisible(true);
		canvas.requestFocusInWindow(); // to receive key events
		setImage(Resources.getDefaultImage());
	}

	private JMenuBar createMenu() {
		var game = menu("Game",
				menuItem("Shuffle", this::shuffle),
				menu("Set Difficulty",
						menuItemDifficulty("3×3 (easy)", 3, 3),
						menuItemDifficulty("3×4 (vertical)", 3, 4),
						menuItemDifficulty("4×3 (horizontal)", 4, 3),
						menuItemDifficulty("4×4 (difficult)", 4, 4),
						menuItemDifficulty("5×5 (extreme)", 5, 5)),
				null,
				menuItem("Import...", this::importGame),
				menuItem("Export", this::exportGame),
				null,
				menuItem("Exit", appFrame::dispose));
		var image = menu("Image",
				menuItem("Open File...", this::openImageFile),
				new JMenuItem("Kittens"));
		var help = menu("Help",
				menuItem("About", this::showAbout),
				menuItem("Instructions", this::showHelp));
		var menuBar = new JMenuBar();
		Stream.of(game, image, help).forEach(menuBar::add);
		return menuBar;
	}

	private JMenu menu(String title, JMenuItem... items) {
		var menu = new JMenu(title);
		for (JMenuItem item : items) {
			if (null == item) {
				menu.addSeparator();
			} else {
				menu.add(item);
			}
		}
		return menu;
	}

	private JMenuItem menuItem(String title, Runnable action) {
		var menuItem = new JMenuItem(title);
		menuItem.addActionListener(e -> action.run());
		return menuItem;
	}

	private JMenuItem menuItemDifficulty(String title, int resolutionX, int resolutionY) {
		return menuItem(title, () -> setDifficulty(resolutionX, resolutionY));
	}

	private Canvas createCanvas() {
		@SuppressWarnings("serial")
		var canvas = new Canvas() {

			@Override
			public void paint(Graphics g) {
				state.drawTo(g, image, pieces);
			}

			@Override
			public void update(Graphics g) {
				paint(g);
			}

		};
		canvas.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent e) {
				if (onKeyPressed(e.getKeyCode())) {
					e.consume();
				}
			}

		});
		return canvas;
	}

	/**
	 * Sets the image that the player tries to solve. If this happens during the game, asks the player whether the game
	 * should be restarted.
	 */
	private void setImage(BufferedImage image) {
		this.image = image;
		pieces = new ImagePieces(image, state.getResolutionX(), state.getResolutionY());
		canvas.setPreferredSize(pieces.getTotalSize());
		appFrame.pack();
		canvas.repaint();
		if (state.isPlaying()) {
			if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(appFrame,
					"Do you want to restart the game?", "Restart game?", JOptionPane.YES_NO_OPTION)) {
				state = new GameState(state.getResolutionX(), state.getResolutionY());
				canvas.repaint();
			}
		}
	}

	/**
	 * Returns true if the key press event is recognized and successfully processed; false otherwise.
	 */
	private boolean onKeyPressed(int keycode) {
		if (KEYS.containsKey(keycode)) {
			if (state.move(KEYS.get(keycode))) {
				canvas.repaint();
				return true;
			}
		}
		return false;
	}

	/**
	 * Shuffles the game pieces.
	 */
	private void shuffle() {
		if (state.isPlaying()) {
			if (JOptionPane.OK_OPTION != JOptionPane.showConfirmDialog(appFrame,
					"This will ruin your current progress. Are you sure?", "Shuffle?", JOptionPane.OK_CANCEL_OPTION)) {
				return;
			}
		}
		state.shuffle();
		canvas.repaint();
	}

	/**
	 * Sets the game resolution.
	 */
	private void setDifficulty(int resolutionX, int resolutionY) {
		if (state.isPlaying()) {
			if (JOptionPane.OK_OPTION != JOptionPane.showConfirmDialog(appFrame,
					"Changing difficulty will restart the game. Are you sure?", "Restart?",
					JOptionPane.OK_CANCEL_OPTION)) {
				return;
			}
		}
		state = new GameState(resolutionX, resolutionY);
		pieces = new ImagePieces(image, resolutionX, resolutionY);
		canvas.setPreferredSize(pieces.getTotalSize());
		appFrame.pack();
	}

	private void exportGame() {
		if (state.isPlaying()) {
			JOptionPane.showInputDialog(appFrame,
					"Copy and save the following text, then close the dialog", state.exportData());
			return;
		}
		JOptionPane.showMessageDialog(appFrame,
				"Game can be exported only while playing", "Not playing", JOptionPane.WARNING_MESSAGE);
	}

	private void importGame() {
		var export = JOptionPane.showInputDialog(appFrame, "Enter the exported data and click OK");
		if (null == export) {
			return;
		}
		try {
			state = GameState.importData(export);
			canvas.repaint();
		} catch (IllegalArgumentException e) {
			JOptionPane.showMessageDialog(appFrame,
					"Cannot import game data: " + e.getMessage(), "Wrong data", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void openImageFile() {
		fileChooser.setDialogTitle("Open image...");
		if (JFileChooser.APPROVE_OPTION != fileChooser.showOpenDialog(appFrame)) {
			return;
		}
		try {
			setImage(ImageIO.read(fileChooser.getSelectedFile()));
		} catch (IOException e) {
			JOptionPane.showMessageDialog(appFrame, e.getMessage(), "Error loading image", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void showAbout() {
		JOptionPane.showMessageDialog(appFrame,
				"Puzzle Game\n\n\u00a9 2021 Kittenlord", "About Puzzle Game", JOptionPane.INFORMATION_MESSAGE);
	}

	private void showHelp() {
		var area = new JTextArea(Resources.getHelpText());
		area.setEditable(false);
		area.setBorder(new EmptyBorder(10, 10, 10, 10));
		var dialog = new JDialog(appFrame, "Instructions");
		var close = new JButton("Close");
		close.addActionListener(e -> dialog.dispose()); // must be before dialog.setVisible
		dialog.add(area);
		dialog.add(close, BorderLayout.PAGE_END);
		dialog.pack();
		dialog.setModalityType(ModalityType.APPLICATION_MODAL);
		dialog.setVisible(true);
	}

}
