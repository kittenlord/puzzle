package kittenlord.puzzle;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

class GameStateTest {

	@Test
	void testExportData() {
		assertThat(new GameState(2, 2).exportData())
				.isEqualTo("2,2," + "0,0,1,0," + "0,1,-1,-1");
		assertThat(new GameState(3, 2).exportData())
				.isEqualTo("3,2," + "0,0,1,0,2,0," + "0,1,1,1,-1,-1");
		assertThat(new GameState(3, 3).exportData())
				.isEqualTo("3,3," + "0,0,1,0,2,0," + "0,1,1,1,2,1," + "0,2,1,2,-1,-1");
	}

	@Test
	void testImportData() {
		var data22 = new GameState(2, 2).exportData();
		assertThat(GameState.importData(data22).exportData()).isEqualTo(data22);
		var data32 = new GameState(3, 2).exportData();
		assertThat(GameState.importData(data32).exportData()).isEqualTo(data32);
		var data33 = new GameState(3, 3).exportData();
		assertThat(GameState.importData(data33).exportData()).isEqualTo(data33);
	}

	/**
	 * We start with pieces rotated counter-clockwise and gradually solve the game.
	 */
	@Test
	void testMove() {
		var state = GameState.importData("2,2," + "1,0,0,1," + "0,0,-1,-1");
		assertThat(state.move(Direction.DOWN)).isTrue();
		assertThat(state.exportData()).isEqualTo("2,2," + "1,0,-1,-1," + "0,0,0,1");
		assertThat(state.move(Direction.RIGHT)).isTrue();
		assertThat(state.exportData()).isEqualTo("2,2," + "-1,-1,1,0," + "0,0,0,1");
		assertThat(state.move(Direction.UP)).isTrue();
		assertThat(state.exportData()).isEqualTo("2,2," + "0,0,1,0," + "-1,-1,0,1");
		assertThat(state.move(Direction.LEFT)).isTrue();
		assertThat(state.exportData()).isEqualTo("2,2," + "0,0,1,0," + "0,1,-1,-1");
		assertThat(state.isPlaying()).isFalse();
	}

}
