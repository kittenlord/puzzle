package kittenlord.puzzle;

import static org.assertj.core.api.Assertions.*;

import java.awt.image.BufferedImage;

import org.junit.jupiter.api.Test;

class ImagePiecesTest {

	@Test
	void testDirRoundUp() {
		assertThat(ImagePieces.divRoundUp(300, 10)).isEqualTo(30);
		assertThat(ImagePieces.divRoundUp(301, 10)).isEqualTo(31);
		assertThat(ImagePieces.divRoundUp(310, 10)).isEqualTo(31);
		assertThat(ImagePieces.divRoundUp(311, 10)).isEqualTo(32);
	}

	@Test
	void testDimensions() {
		var pieces = new ImagePieces(new BufferedImage(25, 25, BufferedImage.TYPE_INT_ARGB), 3, 4);
		assertThat(pieces.getPieceWidth()).isEqualTo(9);
		assertThat(pieces.getTotalWidth()).isEqualTo(27);
		assertThat(pieces.getPieceHeight()).isEqualTo(7);
		assertThat(pieces.getTotalHeight()).isEqualTo(28);
	}
	
}
