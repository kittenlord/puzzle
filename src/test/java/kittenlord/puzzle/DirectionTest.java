package kittenlord.puzzle;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

class DirectionTest {

	@Test
	void testGetX() {
		assertThat(Direction.LEFT.getX()).isEqualTo(-1);
		assertThat(Direction.UP.getX()).isEqualTo(0);
		assertThat(Direction.DOWN.getX()).isEqualTo(0);
		assertThat(Direction.RIGHT.getX()).isEqualTo(1);
	}

	@Test
	void testGetY() {
		assertThat(Direction.UP.getY()).isEqualTo(-1);
		assertThat(Direction.LEFT.getY()).isEqualTo(0);
		assertThat(Direction.RIGHT.getY()).isEqualTo(0);
		assertThat(Direction.DOWN.getY()).isEqualTo(1);
	}

	@Test
	void testGetOpposite() {
		assertThat(Direction.UP.opposite()).isEqualByComparingTo(Direction.DOWN);
		assertThat(Direction.DOWN.opposite()).isEqualByComparingTo(Direction.UP);
		assertThat(Direction.LEFT.opposite()).isEqualByComparingTo(Direction.RIGHT);
		assertThat(Direction.RIGHT.opposite()).isEqualByComparingTo(Direction.LEFT);
	}
	
}
