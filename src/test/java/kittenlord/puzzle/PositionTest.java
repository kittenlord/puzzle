package kittenlord.puzzle;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PositionTest {

	@Test
	void testCoordinate() {
		var c = new Position(3, 7);
		assertThat(c.getX()).isEqualTo(3);
		assertThat(c.getY()).isEqualTo(7);
		assertThat(c.toString()).isEqualTo("(3,7)");
	}

	@Test
	void testNeighbor() {
		var c = new Position(3, 7);
		assertThat(c.neighbor(Direction.UP)).isEqualTo(new Position(3, 6));
		assertThat(c.neighbor(Direction.RIGHT)).isEqualTo(new Position(4, 7));
		assertThat(c.neighbor(Direction.DOWN)).isEqualTo(new Position(3, 8));
		assertThat(c.neighbor(Direction.LEFT)).isEqualTo(new Position(2, 7));
	}

	@Test
	void testRectangle() {
		assertThat(Position.rectangle(3, 2)).containsExactlyInAnyOrder(
				new Position(0, 0), new Position(1, 0), new Position(2, 0),
				new Position(0, 1), new Position(1, 1), new Position(2, 1));
	}

}
